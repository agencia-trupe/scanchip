(function(window, document, $, undefined) {
    'use strict';

    var App = {

        homeCycle: function() {
            var homeSlider = $('.home-slider');

            if (homeSlider.length) {
                homeSlider.cycle({
                    fx: 'fade',
                    speed: 'slow',
                    slides: '> div.slide',
                    autoHeight: 'calc',
                    prev: '#prev',
                    next: '#next'
                });
            }
        },

        produtoCycle: function() {
            var produtoSlider = $('.produto-slider');

            if (produtoSlider.length) {
                produtoSlider.cycle({
                    fx: 'scrollHorz',
                    speed: 'slow',
                    prev: '#prev',
                    next: '#next'
                });
            }
        },

        mostraCaracteristicas: function() {
            var trigger = $('.caracteristicas-trigger'),
                lista   = $('.caracteristicas');

            if (trigger.length) {
                lista.hide();
                trigger.on('click', function(e) {
                    e.preventDefault();
                    lista.slideToggle('normal', function() {
                        trigger.toggleClass('active');
                    });
                });
            }
        },

        selecaoCurriculo: function() {
            var inputCurriculo = $('#curriculo input'),
                spanArquivo    = $('#curriculo span');

            if (inputCurriculo.length) {
                inputCurriculo.on('change', function(e) {
                    var fileName = e.target.files[0].name;
                    spanArquivo.html(fileName).addClass('active');
                });
            }
        },

        init: function() {
            this.homeCycle();
            this.produtoCycle();
            this.mostraCaracteristicas();
            this.selecaoCurriculo();
        }

    };

    $(document).ready(App.init());

}(window, document, jQuery));