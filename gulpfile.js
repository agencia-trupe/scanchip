// Require modules

var env         = require('minimist')(process.argv.slice(2)),
    gulp        = require('gulp'),
    jade        = require('gulp-jade'),
    stylus      = require('gulp-stylus'),
    jeet        = require('jeet'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss');
    jshint      = require('gulp-jshint'),
    uglify      = require('gulp-uglify'),
    concat      = require('gulp-concat'),
    imagemin    = require('gulp-imagemin'),
    gulpif      = require('gulp-if'),
    plumber     = require('gulp-plumber'),
    browserSync = require('browser-sync'),
    reload      = browserSync.reload;


// Path configuration

var path = {
    development: {
        jade   : './src/templates/',
        stylus : './src/stylus/',
        js     : './src/js/',
        img    : './src/img/'
    },

    build: {
        html : './build/',
        css  : './build/assets/css/',
        js   : './build/assets/js/',
        img  : './build/assets/img/'
    }
};


// Compile templates

gulp.task('templateBuild', function(){
    return gulp.src([path.development.jade + '*.jade', '!' + path.development.jade + '_*.jade'])
        .pipe(plumber())
        .pipe(jade({ pretty: env.debug }))
        .pipe(gulp.dest(path.build.html))
        .pipe(reload({ stream: true }))
});


// Compile Stylus

gulp.task('cssBuild', function() {
    return gulp.src([path.development.stylus + 'main.styl'])
        .pipe(plumber())
        .pipe(stylus({
            use: [ koutoSwiss(), jeet(), rupture() ],
            compress: !env.debug
        }))
        .pipe(gulp.dest(path.build.css))
        .pipe(reload({ stream: true }))
});


// Validate JS

gulp.task('jsHint', function() {
    return gulp.src(path.development.js + '**/*.js')
        .pipe(plumber())
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});


// Uglify and concatenate JS

gulp.task('jsBuild', function() {
    return gulp.src(path.development.js + '**/*.js')
        .pipe(plumber())
        .pipe(concat('main.js'))
        .pipe(gulpif(!env.debug, uglify()))
        .pipe(gulp.dest(path.build.js))
        .pipe(reload({ stream: true }))
});


// Optimize images

gulp.task('imageMin', function() {
    return gulp.src(path.development.img + '**/*')
        .pipe(plumber())
        .pipe(imagemin({ progressive: true, interlaced: true }))
        .pipe(gulp.dest(path.build.img))
        .pipe(reload({ stream: true }))
});


// Watch for file changes

gulp.task('watch', function() {
    gulp.watch(path.development.jade + '**/*.jade', ['templateBuild']);
    gulp.watch(path.development.stylus + '**/*.styl', ['cssBuild']);
    gulp.watch(path.development.js + '**/*.js', ['jsHint', 'jsBuild']);
    gulp.watch(path.development.img + '**/*.{jpg,png,gif}', ['imageMin']);
});


// Start browserSync server

gulp.task('browserSync', function() {
    browserSync({
        server: { baseDir: path.build.html }
    });
});


// Default task

gulp.task('default', ['templateBuild', 'cssBuild', 'jsHint', 'jsBuild', 'imageMin', 'watch', 'browserSync']);